export const fetchComputers = () => {
    return `
    {
        "computers": [
          {
            "id": 1,
            "name": "Intol Celeron E10",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
            "price": 1500,
            "features": [
              "Water proof",
              "Heat proof",
              "Shock proof",
              "Snow proof",
              "Wind proof",
              "Space proof"
            ],
            "img": "./Laptop3.png"
          },
          {
            "id": 2,
            "name": "HB Booknote 4",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
            "price": 2400,
            "features": [
              "Boring",
              "Your mom probably owns this",
              "Does computer stuff"
            ],
            "img": "./Laptop4.png"
          },
          {
            "id": 3,
            "name": "NSI Xtreme Gamer 9001",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
            "price": 9999,
            "features": [
              "Maximum FPS",
              "Can run DOOM",
              "Might take off and fly away"
            ],
            "img": "./Laptop1.png"
          },
          {
            "id": 4,
            "name": "ExtraTerrestrialWare X89",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
            "price": 20000,
            "features": [
              "Expensive",
              "From Space",
              "Unknown thecnology",
              "Water proof"
            ],
            "img": "./Laptop2.png"
          }
        ]
      }
    `
}