// Components for reuse

export function createImg(src, imgClass) {
    const laptopImg = document.createElement('img')
    laptopImg.src = src
    laptopImg.className = imgClass
    return laptopImg
}

export function createUl(array) {
    const list = document.createElement('ul')
    array.forEach(el => {
        const li = document.createElement('li')
        li.innerText = el
        list.appendChild(li)
    })
    return list
}

export function createSelect(selectElement, array) {
    const firstOption = document.createElement('option')
    firstOption.innerText = '--Select-a-computer--'
    firstOption.value = -1
    selectElement.appendChild(firstOption)
    array.forEach(comp => {
        const option = document.createElement('option')
        option.innerText = comp.name
        option.value = comp.id
        selectElement.appendChild(option)
    })
}