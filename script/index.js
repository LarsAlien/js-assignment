import {
    fetchComputers
} from './api.js'

import {
    createUl,
    createImg,
    createSelect
} from './components.js'

class App {
    constructor() {
        // Variables
        this.paycheckAmount = 0
        this.debtAmount = 0
        this.balanceAmount = 0
        this.isLoanDone = false
        this.computers = []
        this.selectedComputer = null

        // DOM elements
        this.elPaycheckAmount = document.getElementById('txtPaycheck')
        this.elBtnAddWorkMoney = document.getElementById('btnAddMoneyToPaycheck')
        this.elBtnBankMoney = document.getElementById('btnBankMoney')

        this.elDebtAmount = document.getElementById('txtDebt')
        this.elBalanceAmount = document.getElementById('txtBalance')
        this.elBtnApplyLoan = document.getElementById('btnApplyLoan')
        this.elBtnGetLoan = document.getElementById('btnGetLoan')
        this.elBtnPayDebt = document.getElementById('btnPayDebt')
        this.elInputLoanAmount = document.getElementById('inputLoanAmount')
        this.elAlertGetLoan = document.getElementById('loanAlert')
        this.elModalGetLoan = new bootstrap.Modal(document.getElementById('loanModal'))

        this.elBtnPurchaseLaptop = document.getElementById('btnPurchaseLaptop')
        this.elLblPurchaseButton = document.getElementById('lblPurchaseBtn')
        this.elH4PurchasedComputer = document.getElementById('h4PurchasedComputer')
        this.elModalPurchasedLaptop = new bootstrap.Modal(document.getElementById('modalPurchasedLaptop'))

        this.elStatus = document.getElementById('app-status')
        this.elComputerSelect = document.getElementById('computers')
        this.elSelectedComputer = document.getElementById('selectedComputer')
        this.elImgSelectedLaptop = document.getElementById('laptop_card_img')
        this.elDescriptionSelectedLaptop = document.getElementById('laptop_card_description')
        this.elCardSelectedLaptop = document.getElementById('laptop_card')
    }

    handleComputerSelectOnChange() {
        // Set selected computer
        if (parseInt(this.elComputerSelect.value) === -1) {
            this.elCardSelectedLaptop.style.display = 'none'
            this.elSelectedComputer.innerHTML = ''
            return
        }
        this.selectedComputer = this.computers.find(e => {
            return e.id === parseInt(this.elComputerSelect.value)
        })

        // Append list of features
        this.elSelectedComputer.innerHTML = ''
        const featureList = createUl(this.selectedComputer.features)
        this.elSelectedComputer.appendChild(featureList)

        // Append image and description
        this.elImgSelectedLaptop.innerHTML = ''
        this.elCardSelectedLaptop.style.display = 'inline'
        const laptopImg = createImg(this.selectedComputer.img, 'imgSelectedLaptop')
        this.elImgSelectedLaptop.appendChild(laptopImg)

        this.elDescriptionSelectedLaptop.innerHTML = ''
        const laptopDescription = document.createElement('p')
        laptopDescription.innerText = this.selectedComputer.description
        this.elDescriptionSelectedLaptop.appendChild(laptopDescription)

        //Append price
        this.elLblPurchaseButton.innerText = this.selectedComputer.price + ' kr'
    }

    handleAddWorkMoneyClick() {
        // Add to paycheck balance
        this.paycheckAmount += 100
        //Rerender all dynamic items
        this.render()
    }

    handleBankMoneyClick() {
        // Deduct 10% and transfer to debt-account
        if (this.debtAmount > 0) {
            // Make sure debt doesnt go under 0
            const mathStub = this.debtAmount - (this.paycheckAmount / 10)
            if (mathStub < 0) {
                this.balanceAmount += ((this.paycheckAmount * 0.9) - mathStub)
                this.debtAmount = 0
            } else {
                this.debtAmount = mathStub
                this.balanceAmount += this.paycheckAmount - (this.paycheckAmount / 10)
            }
        } // Transfer paycheck to balance
        else this.balanceAmount += this.paycheckAmount
            
        // Reset paycheck
        this.paycheckAmount = 0
        //Rerender all dynamic items
        this.render()
    }

    handleGetLoanClick() {
        if (this.balanceAmount <= 0 || this.isLoanDone) return
        // Opens loan modal
        this.elModalGetLoan.show()
    }

    handleApplyLoanClick() {
        // Check if loan has been given before
        if (this.isLoanDone || this.balanceAmount === 0) return;
        // Calculate max available loan
        const maxLoanAmount = this.balanceAmount * 2
        const inputLoan = parseInt(this.elInputLoanAmount.value)
        if (inputLoan <= maxLoanAmount) {
            // Add new loan to debt
            this.debtAmount += inputLoan
            // Add new loan to balance
            this.balanceAmount += inputLoan
            // Change loan-boolean to true
            this.isLoanDone = true
            // Display "Pay debt"-button
            this.elBtnPayDebt.style.display = "inline"
            this.elModalGetLoan.hide()
        } else if (inputLoan > maxLoanAmount) {
            this.elAlertGetLoan.innerText = 'The loan amount cant be more than twice your balance'
            this.elAlertGetLoan.style.display = 'inline'
        } else {
            this.elAlertGetLoan.innerText = 'Loan is only done in numbers'
            this.elAlertGetLoan.style.display = 'inline'
        }
        // Rerender
        this.render()
    }

    handlePayDebtClick() {
        // Check if debt is zero
        if (this.debtAmount <= 0 || this.paycheckAmount <= 0) return;
        // Check which amount is highest, to determine what to subtract
        if (this.debtAmount <= this.paycheckAmount) {
            this.paycheckAmount = this.paycheckAmount - this.debtAmount
            this.debtAmount = 0
        }else if (this.paycheckAmount < this.debtAmount) {
            this.debtAmount = this.debtAmount - this.paycheckAmount
            this.paycheckAmount = 0
        }
        // Check if full debt is payed and hide "Pay debt"-button
        this.elBtnPayDebt.style.display = "none"
        // Rerender
        this.render()
    }

    handlePurchaseLaptopClick() {
        if (this.balanceAmount >= this.selectedComputer.price) {
            this.balanceAmount = this.balanceAmount - this.selectedComputer.price
            this.elH4PurchasedComputer.innerText = 'Congratulations on buying the: ' + this.selectedComputer.name
            this.elModalPurchasedLaptop.show()
            this.isLoanDone = false
        } else {
            this.elH4PurchasedComputer.innerText = 'Not enough cash.....'
            this.elModalPurchasedLaptop.show()
        }
        this.render()
    }

    async init() {
        // Listen for events
        this.elComputerSelect.addEventListener('change', this.handleComputerSelectOnChange.bind(this))
        this.elBtnAddWorkMoney.addEventListener('click', this.handleAddWorkMoneyClick.bind(this))
        this.elBtnBankMoney.addEventListener('click', this.handleBankMoneyClick.bind(this))
        this.elBtnApplyLoan.addEventListener('click', this.handleApplyLoanClick.bind(this))
        this.elBtnGetLoan.addEventListener('click', this.handleGetLoanClick.bind(this))
        this.elBtnPayDebt.addEventListener('click', this.handlePayDebtClick.bind(this))
        this.elBtnPurchaseLaptop.addEventListener('click', this.handlePurchaseLaptopClick.bind(this))


        this.elStatus.innerText = 'Loading comps...'
        this.elComputerSelect.disabled = true
        try {
            this.computers = JSON.parse(fetchComputers()).computers
            this.elStatus.innerText = ''
        } catch (e) {
            this.elStatus.innerText = 'Error' + e.message
        } finally {
            this.elComputerSelect.disabled = false
        }

        createSelect(this.elComputerSelect, this.computers)

        this.render()
    }

    render() {
        // Put values
        this.elPaycheckAmount.innerText = this.paycheckAmount + ' kr'
        if (this.debtAmount > 0) {
            this.elDebtAmount.style.display = 'inline'
            this.elDebtAmount.innerText = 'Debt: ' + this.debtAmount + ' kr'
        } else this.elDebtAmount.style.display = 'none'
        this.elBalanceAmount.innerText = this.balanceAmount + ' kr'
    }
}

new App().init()